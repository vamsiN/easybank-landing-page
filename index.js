let imageElement = document.getElementById("hamburgerImage");
let click = true;
imageElement.onclick = function () {
 
    if (click === true) {
        imageElement.src = './images/icon-close.svg';
        click = false;
    } else {
        imageElement.src = './images/icon-hamburger.svg';
        click = true;
    }
}